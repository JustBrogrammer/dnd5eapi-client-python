# DnD 5th Edition API - Python client

A Python client to interact with the D&D 5th Edition API ([homepage](https://www.dnd5eapi.co)).

## License

![LGPLv3 logo](https://www.gnu.org/graphics/lgplv3-147x51.png)

[GNU Lesser General Public License v3.0](https://www.gnu.org/licenses/lgpl-3.0.en.html) (LGPL-3.0-only)

```
This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License, version 3, as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>. 
```
